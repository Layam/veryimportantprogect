class CreateMissingRequests < ActiveRecord::Migration
  def change
    create_table :missing_requests do |t|
      t.string :text
      t.string :comment
      t.boolean :status

      t.timestamps null: false
    end
  end
end
