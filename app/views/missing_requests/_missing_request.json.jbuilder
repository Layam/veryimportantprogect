json.extract! missing_request, :id, :text, :comment, :status, :created_at, :updated_at
json.url missing_request_url(missing_request, format: :json)