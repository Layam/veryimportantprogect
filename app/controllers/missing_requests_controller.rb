class MissingRequestsController < ApplicationController
  before_action :require_user
  before_action :set_missing_request, only: [:show, :edit, :update, :destroy, :close]
  # TODO: use cancan or smt like that for protect page

  # GET /missing_requests
  # GET /missing_requests.json
  def index
    @missing_requests = MissingRequest.all
  end

  # GET /missing_requests/1
  # GET /missing_requests/1.json
  def show
  end

  # GET /missing_requests/new
  def new
    @missing_request = MissingRequest.new
  end

  # GET /missing_requests/1/edit
  def edit
  end

  # POST /missing_requests
  # POST /missing_requests.json
  def create
    @missing_request = MissingRequest.new(missing_request_params)
    @missing_request[:status] = false

    respond_to do |format|
      if @missing_request.save
        format.html { redirect_to @missing_request, notice: 'Missing request was successfully created.' }
        format.json { render :show, status: :created, location: @missing_request }
      else
        format.html { render :new }
        format.json { render json: @missing_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /missing_requests/1
  # PATCH/PUT /missing_requests/1.json
  def update
    respond_to do |format|
      if @missing_request.update(missing_request_params)
        format.html { redirect_to @missing_request, notice: 'Missing request was successfully updated.' }
        format.json { render :show, status: :ok, location: @missing_request }
      else
        format.html { render :edit }
        format.json { render json: @missing_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /missing_requests/1
  # DELETE /missing_requests/1.json
  def destroy
    @missing_request.destroy
    respond_to do |format|
      format.html { redirect_to missing_requests_url, notice: 'Missing request was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # POST /missing_requests/1/close
  def close
    @missing_request.update_attributes(status: true)
    redirect_to(@missing_request)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_missing_request
      @missing_request = MissingRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def missing_request_params
      params.require(:missing_request).permit(:text, :comment, :status)
    end
end
