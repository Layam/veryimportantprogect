Rails.application.routes.draw do

  resources :missing_requests do
    member do
      post :close
    end
  end
  get 'signup'  => 'users#new'
  resources :users

  get 'login' => 'sessions#new' # метод new создает и сразу возвращает новый объект?
  post 'login' => 'sessions#create'
  post 'logout' => 'sessions#destroy'

  get 'close' => 'missing_requests'

  get 'missing_requests/:id/close' => 'session#new'

  root 'sessions#new'
end
