require 'test_helper'

class MissingRequestsControllerTest < ActionController::TestCase
  setup do
    @missing_request = missing_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:missing_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create missing_request" do
    assert_difference('MissingRequest.count') do
      post :create, missing_request: { comment: @missing_request.comment, status: @missing_request.status, text: @missing_request.text }
    end

    assert_redirected_to missing_request_path(assigns(:missing_request))
  end

  test "should show missing_request" do
    get :show, id: @missing_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @missing_request
    assert_response :success
  end

  test "should update missing_request" do
    patch :update, id: @missing_request, missing_request: { comment: @missing_request.comment, status: @missing_request.status, text: @missing_request.text }
    assert_redirected_to missing_request_path(assigns(:missing_request))
  end

  test "should destroy missing_request" do
    assert_difference('MissingRequest.count', -1) do
      delete :destroy, id: @missing_request
    end

    assert_redirected_to missing_requests_path
  end
end
